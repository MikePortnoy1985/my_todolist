import React, { useEffect, useState } from 'react'
import { tasksAPI, UpdateTaskType } from '../api/tasks-api'

export default {
   title: 'Tasks API',
}

export const GetTasks = () => {
   const [state, setState] = useState<any>(null)
   useEffect(() => {
      const todolistID = 'f6daf50b-6601-4dbc-9337-9016cf538181'
      tasksAPI.getTasks(todolistID).then((res) => {
         setState(res.data)
      })
   }, [])

   return <div> {JSON.stringify(state)}</div>
}
export const CreateTask = () => {
   const [state, setState] = useState<any>(null)
   useEffect(() => {
      const todolistID = 'f6daf50b-6601-4dbc-9337-9016cf538181'
      const title = 'NewApi'
      tasksAPI.createTask(todolistID, title).then((res) => {
         setState(res.data)
      })
   }, [])

   return <div> {JSON.stringify(state)}</div>
}
export const DeleteTask = () => {
   const [state, setState] = useState<any>(null)
   useEffect(() => {
      const todolistID = 'f6daf50b-6601-4dbc-9337-9016cf538181'
      const taskId = 'd52c8dae-7af5-47b7-a603-d047fbbf89f6'
      tasksAPI.deleteTask(todolistID, taskId).then((res) => {
         setState(res.data)
      })
   }, [])

   return <div> {JSON.stringify(state)}</div>
}
export const UpdateTask = () => {
   const [state, setState] = useState<any>(null)
   const [todolistID, setTodolistID] = useState<any>(null)
   const [taskID, setTaskID] = useState<any>(null)
   const [title, setTitle] = useState<any>(null)
   const onServer = () => {
      const model: UpdateTaskType = {
         title: title,
         description: '',
         status: 0,
         priority: 1,
         startDate: '',
         deadline: '',
      }
      tasksAPI.updateTask(todolistID, taskID, model).then((res) => {
         setState(res.data)
      })
   }

   return (
      <>
         <input placeholder={'id'} value={todolistID} onChange={(e) => setTodolistID(e.currentTarget.value)} />
         <input placeholder={'taskId'} value={taskID} onChange={(e) => setTaskID(e.currentTarget.value)} />
         <input placeholder={'title'} value={title} onChange={(e) => setTitle(e.currentTarget.value)} />
         <button onClick={onServer}>update</button>
         <div> {JSON.stringify(state)}</div>
      </>
   )
}
