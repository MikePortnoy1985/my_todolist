import React, { useEffect, useState } from 'react'
import { appTodolistsApi } from '../api/appTodolists-api'

export default {
   title: 'Todolist API',
}

export const GetTodolists = () => {
   const [state, setState] = useState<any>(null)
   useEffect(() => {
      appTodolistsApi.getTodolists().then((res) => {
         setState(res.data)
      })
   }, [])

   return <div> {JSON.stringify(state)}</div>
}
export const CreateTodolist = () => {
   const [state, setState] = useState<any>(null)
   useEffect(() => {
      const title = 'NewApi'
      appTodolistsApi.createTodolist(title).then((res) => {
         setState(res.data)
      })
   }, [])

   return <div> {JSON.stringify(state)}</div>
}
export const DeleteTodolist = () => {
   const [state, setState] = useState<any>(null)
   useEffect(() => {
      const todolistID = '767a1f2f-4382-46c7-b5a0-f73602c21dcf'
      appTodolistsApi.deleteTodolist(todolistID).then((res) => {
         setState(res.data)
      })
   }, [])

   return <div> {JSON.stringify(state)}</div>
}
export const UpdateTodolistTitle = () => {
   const [state, setState] = useState<any>(null)
   useEffect(() => {
      const title = 'APIII'
      const todolistID = '767a1f2f-4382-46c7-b5a0-f73602c21dcf'
      appTodolistsApi.updateTodolistTitle(title, todolistID).then((res) => {
         setState(res.data)
      })
   }, [])

   return <div> {JSON.stringify(state)}</div>
}
