import React from 'react'
import { Provider } from 'react-redux'
import { AppRootStateType } from '../app/store'
import { applyMiddleware, combineReducers, createStore } from 'redux'
import { tasksReducer } from '../features/TodolistsList/tasks-reducer'
import { todolistsReducer } from '../features/TodolistsList/todolists-reducer'
import { v1 } from 'uuid'
import { TaskPriority, TaskStatuses } from '../api/tasks-api'
import { appReducer } from '../app/app-reducer'
import thunk from 'redux-thunk'
import { authReducer } from '../features/Login/auth-reducer'

const rootReducer = combineReducers({
   tasks: tasksReducer,
   todolists: todolistsReducer,
   app: appReducer,
   authReducer: authReducer,
})

const initialGlobalState = {
   todolists: [
      { id: 'todolistId1', title: 'What to learn', filter: 'all', addedDate: '', order: 0, entityStatus: 'idle' },
      { id: 'todolistId2', title: 'What to buy', filter: 'all', addedDate: '', order: 1, entityStatus: 'idle' },
   ],
   tasks: {
      todolistId1: [
         {
            id: v1(),
            title: 'HTML&CSS',
            status: TaskStatuses.New,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId1',
         },
         {
            id: v1(),
            title: 'JS',
            status: TaskStatuses.New,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId1',
         },
      ],
      todolistId2: [
         {
            id: v1(),
            title: 'Milk',
            status: TaskStatuses.New,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId2',
         },
         {
            id: v1(),
            title: 'React Book',
            status: TaskStatuses.New,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId2',
         },
      ],
   },
   app: {
      status: 'idle',
      error: null,
      isInitialized: false,
   },
   authReducer: {
      isLoggedIn: false,
   },
}

export const storyBookStore = createStore(rootReducer, initialGlobalState as AppRootStateType, applyMiddleware(thunk))

const ProviderDecorator = (story: any) => {
   return <Provider store={storyBookStore}>{story()}</Provider>
}

export default ProviderDecorator
