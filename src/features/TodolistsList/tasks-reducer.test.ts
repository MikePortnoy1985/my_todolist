import { tasksReducer, addTaskAC, removeTaskAC, setTasksAC, TasksStateType, updateTaskAC } from './tasks-reducer'
import { addTodolistAC, removeTodolistAC, setTodolistsAC } from './todolists-reducer'
import { TaskPriority, TaskStatuses } from '../../api/tasks-api'

let startState: TasksStateType = {}

beforeEach(() => {
   startState = {
      todolistId1: [
         {
            id: '1',
            title: 'CSS',
            status: TaskStatuses.New,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId1',
         },
         {
            id: '2',
            title: 'JS',
            status: TaskStatuses.Completed,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId1',
         },
         {
            id: '3',
            title: 'React',
            status: TaskStatuses.New,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId1',
         },
      ],
      todolistId2: [
         {
            id: '1',
            title: 'bread',
            status: TaskStatuses.New,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId2',
         },
         {
            id: '2',
            title: 'milk',
            status: TaskStatuses.Completed,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId2',
         },
         {
            id: '3',
            title: 'tea',
            status: TaskStatuses.New,
            description: '',
            addedDate: '',
            deadline: '',
            order: 0,
            startDate: '',
            priority: TaskPriority.Low,
            todoListId: 'todolistId2',
         },
      ],
   }
})

test('correct task should be deleted from correct array', () => {
   const action = removeTaskAC({ taskId: '2', todolistId: 'todolistId2' })

   const endState = tasksReducer(startState, action)

   expect(endState['todolistId1'].length).toBe(3)
   expect(endState['todolistId2'].length).toBe(2)
   expect(endState['todolistId2'].every((t) => t.id !== '2')).toBeTruthy()
})

test('correct task should be added to correct array', () => {
   const action = addTaskAC({
      task: {
         id: '3',
         title: 'juice',
         status: TaskStatuses.New,
         description: '',
         addedDate: '',
         deadline: '',
         order: 0,
         startDate: '',
         priority: TaskPriority.Low,
         todoListId: 'todolistId2',
      },
   })

   const endState = tasksReducer(startState, action)

   expect(endState['todolistId1'].length).toBe(3)
   expect(endState['todolistId2'].length).toBe(4)
   expect(endState['todolistId2'][0].id).toBeDefined()
   expect(endState['todolistId2'][0].title).toBe('juice')
   expect(endState['todolistId2'][0].status).toBe(TaskStatuses.New)
})

test('status of specified task should be changed', () => {
   const action = updateTaskAC({ taskId: '2', model: { status: TaskStatuses.Completed }, todolistId: 'todolistId2' })

   const endState = tasksReducer(startState, action)

   expect(endState['todolistId2'][1].status).toBe(TaskStatuses.Completed)
   expect(endState['todolistId1'][1].status).toBe(TaskStatuses.Completed)
})

test('title of specified task should be changed', () => {
   const action = updateTaskAC({ taskId: '2', model: { title: 'butter' }, todolistId: 'todolistId2' })

   const endState = tasksReducer(startState, action)

   expect(endState['todolistId2'][1].title).toBe('butter')
   expect(endState['todolistId1'][1].title).toBe('JS')
})

test('new array should be added when new todolist is added', () => {
   const action = addTodolistAC({
      todolist: {
         id: '123',
         title: 'test',
         order: 0,
         addedDate: '',
      },
   })

   const endState = tasksReducer(startState, action)

   const keys = Object.keys(endState)
   const newKey = keys.find((k) => k !== 'todolistId1' && k !== 'todolistId2')
   if (!newKey) {
      throw Error('new key should be added')
   }

   expect(keys.length).toBe(3)
   expect(endState[newKey]).toEqual([])
})

test('property with todolistId should be deleted', () => {
   const action = removeTodolistAC({ todolistId: 'todolistId2' })

   const endState = tasksReducer(startState, action)

   const keys = Object.keys(endState)

   expect(keys.length).toBe(1)
   expect(endState['todolistId2']).not.toBeDefined()
})

test('empty array should be added when we set todolists', () => {
   const action = setTodolistsAC({
      todolists: [
         { id: '1', title: 'title 1', addedDate: '', order: 0 },
         { id: '2', title: 'title 2', addedDate: '', order: 1 },
      ],
   })

   const endState = tasksReducer({}, action)

   const keys = Object.keys(endState)

   expect(keys.length).toBe(2)
   expect(endState['1']).toStrictEqual([])
   expect(endState['2']).toStrictEqual([])
})

test('tasks should be added to todolist', () => {
   const action = setTasksAC({ tasks: startState['todolistId1'], todolistId: 'todolistId1' })

   const endState = tasksReducer(
      {
         todolistId2: [],
         todolistId1: [],
      },
      action,
   )

   expect(endState['todolistId1'].length).toBe(3)
   expect(endState['todolistId2'].length).toBe(0)
})
