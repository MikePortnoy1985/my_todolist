import {
   todolistsReducer,
   addTodolistAC,
   changeTodolistEntityStatusAC,
   changeTodolistFilterAC,
   changeTodolistTitleAC,
   FilterValuesType,
   removeTodolistAC,
   setTodolistsAC,
   TodolistDomainType,
} from './todolists-reducer'
import { v1 } from 'uuid'
import { RequestStatusType } from '../../app/app-reducer'

let todolistId1: string
let todolistId2: string
let startState: Array<TodolistDomainType> = []

beforeEach(() => {
   todolistId1 = v1()
   todolistId2 = v1()
   startState = [
      { id: todolistId1, title: 'What to learn', addedDate: '', order: 0, filter: 'all', entityStatus: 'idle' },
      { id: todolistId2, title: 'What to buy', addedDate: '', order: 1, filter: 'all', entityStatus: 'idle' },
   ]
})
const newTodolistTitle = 'New Todolist'

test('correct todolist should be removed', () => {
   const endState = todolistsReducer(startState, removeTodolistAC({ todolistId: todolistId1 }))

   expect(endState.length).toBe(1)
   expect(endState[0].id).toBe(todolistId2)
})

test('correct todolist should be added', () => {
   const endState = todolistsReducer(
      startState,
      addTodolistAC({
         todolist: {
            id: 'qweq',
            title: newTodolistTitle,
            addedDate: '',
            order: 0,
         },
      }),
   )

   expect(endState.length).toBe(3)
   expect(endState[0].title).toBe(newTodolistTitle)
   expect(endState[0].filter).toBe('all')
})

test('correct todolist should change its name', () => {
   const endState = todolistsReducer(
      startState,
      changeTodolistTitleAC({ todolistId: todolistId2, title: newTodolistTitle }),
   )

   expect(endState[0].title).toBe('What to learn')
   expect(endState[1].title).toBe(newTodolistTitle)
})

test('correct filter of todolist should be changed', () => {
   const newFilter: FilterValuesType = 'completed'

   const endState = todolistsReducer(startState, changeTodolistFilterAC({ todolistId: todolistId2, filter: newFilter }))

   expect(endState[0].filter).toBe('all')
   expect(endState[1].filter).toBe(newFilter)
})

test('todolists should be set to the state', () => {
   const action = setTodolistsAC({ todolists: startState })

   const endState = todolistsReducer([], action)

   expect(endState.length).toBe(2)
})

test('correct status of todolist should be changed', () => {
   const newStatus: RequestStatusType = 'loading'

   const endState = todolistsReducer(
      startState,
      changeTodolistEntityStatusAC({ todolistId: todolistId2, entityStatus: newStatus }),
   )

   expect(endState[0].entityStatus).toBe('idle')
   expect(endState[1].entityStatus).toBe(newStatus)
})
