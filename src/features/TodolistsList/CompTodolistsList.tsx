import { useDispatch, useSelector } from 'react-redux'
import { AppRootStateType } from '../../app/store'
import {
   addTodolistTC,
   changeTodolistFilterAC,
   changeTodolistTitleTC,
   deleteTodolistTC,
   fetchTodolistsTC,
   FilterValuesType,
   TodolistDomainType,
} from './todolists-reducer'
import React, { useCallback, useEffect } from 'react'
import { Container, Grid, Paper } from '@material-ui/core'
import { AddItemForm } from '../../components/AddItemForm/AddItemForm'
import { ComponentTodolist } from './Todolist/ComponentTodolist'
import { Redirect } from 'react-router-dom'

const TodolistsList = () => {
   const dispatch = useDispatch()
   const todolists = useSelector<AppRootStateType, Array<TodolistDomainType>>((state) => state.todolists)
   const isLoggedIn = useSelector<AppRootStateType, boolean>((state) => state.authReducer.isLoggedIn)

   useEffect(() => {
      if (!isLoggedIn) {
         return
      }
      dispatch(fetchTodolistsTC())
   }, [dispatch, isLoggedIn])

   const changeFilter = useCallback(
      (value: FilterValuesType, todolistId: string) => {
         dispatch(changeTodolistFilterAC({ todolistId, filter: value }))
      },
      [dispatch],
   )

   const removeTodolist = useCallback(
      (todolistId: string) => {
         dispatch(deleteTodolistTC(todolistId))
      },
      [dispatch],
   )

   const addTodolist = useCallback(
      (title: string) => {
         dispatch(addTodolistTC(title))
      },
      [dispatch],
   )

   const changeTodolistTitle = useCallback(
      (newTitle: string, todolistId: string) => {
         dispatch(changeTodolistTitleTC(newTitle, todolistId))
      },
      [dispatch],
   )

   if (!isLoggedIn) {
      return <Redirect to={'/login'} />
   }

   return (
      <>
         <Container fixed>
            <Grid key={1} container style={{ padding: '20px' }}>
               <AddItemForm addItem={addTodolist} />
            </Grid>
            <Grid key={2} container spacing={3}>
               {todolists.map((tl) => {
                  return (
                     <Grid item>
                        <Paper elevation={3} style={{ padding: '10px 20px' }}>
                           <ComponentTodolist
                              key={tl.id}
                              todolist={tl}
                              changeFilter={changeFilter}
                              removeTodolist={removeTodolist}
                              changeTodolistTitle={changeTodolistTitle}
                           />
                        </Paper>
                     </Grid>
                  )
               })}
            </Grid>
         </Container>
      </>
   )
}

export default TodolistsList
