import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Dispatch } from 'redux'
import { TodolistType } from '../../api/appTodolists-api'
import { TaskPriority, tasksAPI, TaskStatuses, TaskType, UpdateTaskType } from '../../api/tasks-api'
import { setStatusAC } from '../../app/app-reducer'
import { AppRootStateType } from '../../app/store'
import { handleServerAppError, handleServerNetworkError } from '../../utils/handle-server-error'
import { addTodolistAC, removeTodolistAC, setTodolistsAC } from './todolists-reducer'

export type TasksStateType = {
   [key: string]: Array<TaskType>
}

const taskSLice = createSlice({
   name: 'tasks',
   initialState: {} as TasksStateType,
   reducers: {
      removeTaskAC(state, action: PayloadAction<{ taskId: string; todolistId: string }>) {
         const todolist = state[action.payload.todolistId]
         const index = todolist.findIndex((t) => t.id === action.payload.taskId)
         todolist.splice(index, 1)
      },
      addTaskAC(state, action: PayloadAction<{ task: TaskType }>) {
         state[action.payload.task.todoListId].unshift(action.payload.task)
      },
      updateTaskAC(state, action: PayloadAction<{ taskId: string; model: UpdateTaskDomainType; todolistId: string }>) {
         const todolist = state[action.payload.todolistId]
         const index = todolist.findIndex((t) => t.id === action.payload.taskId)
         if (index > -1) {
            todolist[index] = { ...todolist[index], ...action.payload.model }
         }
      },
      setTasksAC(state, action: PayloadAction<{ tasks: Array<TaskType>; todolistId: string }>) {
         state[action.payload.todolistId] = action.payload.tasks
      },
   },
   extraReducers: (builder) => {
      builder.addCase(addTodolistAC, (state, action) => {
         state[action.payload.todolist.id] = []
      })
      builder.addCase(removeTodolistAC, (state, action) => {
         delete state[action.payload.todolistId]
      })
      builder.addCase(setTodolistsAC, (state, action) => {
         action.payload.todolists.forEach((tl: TodolistType) => (state[tl.id] = []))
      })
   },
})

export const tasksReducer = taskSLice.reducer
export const { removeTaskAC, addTaskAC, updateTaskAC, setTasksAC } = taskSLice.actions

export const fetchTasksTC = (todolistId: string) => async (dispatch: Dispatch) => {
   try {
      dispatch(setStatusAC({ status: 'loading' }))
      const response = await tasksAPI.getTasks(todolistId)
      dispatch(setTasksAC({ tasks: response.data.items, todolistId }))
      dispatch(setStatusAC({ status: 'succeeded' }))
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}
export const deleteTaskTC = (todolistId: string, taskId: string) => async (dispatch: Dispatch) => {
   try {
      await tasksAPI.deleteTask(todolistId, taskId)
      dispatch(removeTaskAC({ taskId, todolistId }))
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}
export const addTaskTC = (taskTitle: string, todolistId: string) => async (dispatch: Dispatch) => {
   try {
      dispatch(setStatusAC({ status: 'loading' }))
      const response = await tasksAPI.createTask(todolistId, taskTitle)
      if (response.data.resultCode === 0) {
         dispatch(addTaskAC({ task: response.data.data.item }))
         dispatch(setStatusAC({ status: 'succeeded' }))
      } else {
         handleServerAppError(response.data, dispatch)
      }
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}

type UpdateTaskDomainType = {
   title?: string
   description?: string
   status?: TaskStatuses
   priority?: TaskPriority
   startDate?: string
   deadline?: string
}

export const updateTaskTC = (taskId: string, updateModel: UpdateTaskDomainType, todolistId: string) => async (
   dispatch: Dispatch,
   getState: () => AppRootStateType,
) => {
   try {
      const state = getState()
      const task = state.tasks[todolistId].find((t) => t.id === taskId)
      if (!task) {
         throw new Error('Task not found')
      }
      const model: UpdateTaskType = {
         title: task.title,
         description: task.description,
         status: task.status,
         priority: task.priority,
         startDate: task.startDate,
         deadline: task.deadline,
         ...updateModel,
      }
      const response = await tasksAPI.updateTask(todolistId, taskId, model)
      if (response.data.resultCode === 0) {
         dispatch(updateTaskAC({ taskId, model: updateModel, todolistId }))
      } else {
         handleServerAppError(response.data, dispatch)
      }
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}
