import { action } from '@storybook/addon-actions'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { appTodolistsApi, TodolistType } from '../../api/appTodolists-api'
import { Dispatch } from 'redux'
import { RequestStatusType, setStatusAC } from '../../app/app-reducer'
import { handleServerNetworkError } from '../../utils/handle-server-error'

export type FilterValuesType = 'all' | 'completed' | 'active'
export type TodolistDomainType = TodolistType & {
   filter: FilterValuesType
   entityStatus: RequestStatusType
}

const todolistSlice = createSlice({
   name: 'todolists',
   initialState: [] as Array<TodolistDomainType>,
   reducers: {
      removeTodolistAC(state, action: PayloadAction<{ todolistId: string }>) {
         const index = state.findIndex((tl) => tl.id === action.payload.todolistId)
         if (index > -1) {
            state.splice(index, 1)
         }
      },
      addTodolistAC(state, action: PayloadAction<{ todolist: TodolistType }>) {
         state.unshift({ ...action.payload.todolist, filter: 'all', entityStatus: 'idle' })
      },
      changeTodolistTitleAC(state, action: PayloadAction<{ todolistId: string; title: string }>) {
         const index = state.findIndex((tl) => tl.id === action.payload.todolistId)
         if (index > -1) {
            state[index].title = action.payload.title
         }
      },
      changeTodolistFilterAC(state, action: PayloadAction<{ todolistId: string; filter: FilterValuesType }>) {
         const index = state.findIndex((tl) => tl.id === action.payload.todolistId)
         if (index > -1) {
            state[index].filter = action.payload.filter
         }
      },
      changeTodolistEntityStatusAC(
         state,
         action: PayloadAction<{ todolistId: string; entityStatus: RequestStatusType }>,
      ) {
         const index = state.findIndex((tl) => tl.id === action.payload.todolistId)
         if (index > -1) {
            state[index].entityStatus = action.payload.entityStatus
         }
      },
      setTodolistsAC(state, action: PayloadAction<{ todolists: Array<TodolistType> }>) {
         return action.payload.todolists.map((tl) => ({ ...tl, filter: 'all', entityStatus: 'idle' }))
      },
   },
})

export const todolistsReducer = todolistSlice.reducer
export const {
   removeTodolistAC,
   addTodolistAC,
   changeTodolistTitleAC,
   changeTodolistFilterAC,
   changeTodolistEntityStatusAC,
   setTodolistsAC,
} = todolistSlice.actions

export const fetchTodolistsTC = () => async (dispatch: Dispatch) => {
   try {
      dispatch(setStatusAC({ status: 'loading' }))
      const response = await appTodolistsApi.getTodolists()
      dispatch(setTodolistsAC({ todolists: response.data }))
      dispatch(setStatusAC({ status: 'succeeded' }))
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}

export const deleteTodolistTC = (todolistId: string) => async (dispatch: Dispatch) => {
   try {
      dispatch(setStatusAC({ status: 'loading' }))
      dispatch(changeTodolistEntityStatusAC({ todolistId, entityStatus: 'loading' }))
      await appTodolistsApi.deleteTodolist(todolistId)
      dispatch(removeTodolistAC({ todolistId }))
      dispatch(changeTodolistEntityStatusAC({ todolistId, entityStatus: 'succeeded' }))
      dispatch(setStatusAC({ status: 'succeeded' }))
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}

export const addTodolistTC = (title: string) => async (dispatch: Dispatch) => {
   try {
      dispatch(setStatusAC({ status: 'loading' }))
      const response = await appTodolistsApi.createTodolist(title)
      dispatch(addTodolistAC({ todolist: response.data.data.item }))
      dispatch(setStatusAC({ status: 'succeeded' }))
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}

export const changeTodolistTitleTC = (title: string, todolistId: string) => async (dispatch: Dispatch) => {
   try {
      await appTodolistsApi.updateTodolistTitle(title, todolistId)
      dispatch(changeTodolistTitleAC({ todolistId, title }))
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}
