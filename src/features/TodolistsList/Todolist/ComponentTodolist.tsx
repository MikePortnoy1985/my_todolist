import React, { useCallback, useEffect } from 'react'
import '../../../app/App.css'
import { AddItemForm } from '../../../components/AddItemForm/AddItemForm'
import { EditableSpan } from '../../../components/EditableSpan/EditableSpan'
import { Button, IconButton } from '@material-ui/core'
import { Delete } from '@material-ui/icons'
import { useDispatch, useSelector } from 'react-redux'
import { AppRootStateType } from '../../../app/store'
import { addTaskTC, fetchTasksTC } from '../tasks-reducer'
import Task from '../Task/Task'
import { FilterValuesType, TodolistDomainType } from '../todolists-reducer'
import { TaskStatuses, TaskType } from '../../../api/tasks-api'

type PropsType = {
   todolist: TodolistDomainType
   changeFilter: (value: FilterValuesType, todolistId: string) => void
   removeTodolist: (todolistId: string) => void
   changeTodolistTitle: (newTitle: string, todolistId: string) => void
}

export const ComponentTodolist = React.memo(
   ({ todolist, changeFilter, removeTodolist, changeTodolistTitle }: PropsType) => {
      const dispatch = useDispatch()
      let tasks = useSelector<AppRootStateType, TaskType[]>((state) => state.tasks[todolist.id])

      const addTask = useCallback(
         (title: string) => {
            dispatch(addTaskTC(title, todolist.id))
         },
         [todolist.id, dispatch],
      )

      useEffect(() => {
         dispatch(fetchTasksTC(todolist.id))
      }, [todolist.id, dispatch])

      const onAllClickHandler = useCallback(() => changeFilter('all', todolist.id), [changeFilter, todolist.id])
      const onActiveClickHandler = useCallback(() => changeFilter('active', todolist.id), [changeFilter, todolist.id])
      const onCompletedClickHandler = useCallback(() => changeFilter('completed', todolist.id), [
         changeFilter,
         todolist.id,
      ])

      const removeTodoList = useCallback(() => {
         removeTodolist(todolist.id)
      }, [removeTodolist, todolist.id])

      const changeTodoListTitle = useCallback(
         (newTitle: string) => {
            changeTodolistTitle(newTitle, todolist.id)
         },
         [changeTodolistTitle, todolist.id],
      )

      if (todolist.filter === 'active') {
         tasks = tasks.filter((t) => t.status !== TaskStatuses.Completed)
      }
      if (todolist.filter === 'completed') {
         tasks = tasks.filter((t) => t.status === TaskStatuses.Completed)
      }

      return (
         <div>
            <h3>
               <EditableSpan title={todolist.title} onChange={changeTodoListTitle} />
               <IconButton size={'small'} onClick={removeTodoList} disabled={todolist.entityStatus === 'loading'}>
                  <Delete fontSize={'small'} />
               </IconButton>
            </h3>
            <AddItemForm addItem={addTask} disabled={todolist.entityStatus === 'loading'} />
            <div>
               {tasks.map((t) => (
                  <Task key={t.id} t={t} todolistId={todolist.id} />
               ))}
            </div>
            <div>
               <Button variant={todolist.filter === 'all' ? 'contained' : 'text'} onClick={onAllClickHandler}>
                  All
               </Button>
               <Button
                  color={'primary'}
                  variant={todolist.filter === 'active' ? 'contained' : 'text'}
                  onClick={onActiveClickHandler}>
                  Active
               </Button>
               <Button
                  color={'secondary'}
                  variant={todolist.filter === 'completed' ? 'contained' : 'text'}
                  onClick={onCompletedClickHandler}>
                  Completed
               </Button>
            </div>
         </div>
      )
   },
)
