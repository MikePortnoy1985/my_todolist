import { deleteTaskTC, updateTaskTC } from '../tasks-reducer'
import React, { ChangeEvent, useCallback } from 'react'
import { Checkbox, IconButton } from '@material-ui/core'
import { EditableSpan } from '../../../components/EditableSpan/EditableSpan'
import { Delete } from '@material-ui/icons'
import { useDispatch } from 'react-redux'
import { TaskStatuses, TaskType } from '../../../api/tasks-api'

type PropsType = {
   t: TaskType
   todolistId: string
}

export const Task = React.memo((props: PropsType) => {
   const dispatch = useDispatch()

   const onRemoveTaskHandler = () => dispatch(deleteTaskTC(props.todolistId, props.t.id))

   const onChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
      const status = e.currentTarget.checked ? TaskStatuses.Completed : TaskStatuses.New
      dispatch(updateTaskTC(props.t.id, { status }, props.todolistId))
   }

   const onChangeTitleHandler = useCallback(
      (newValue: string) => {
         dispatch(updateTaskTC(props.t.id, { title: newValue }, props.todolistId))
      },
      [props.t.id, props.todolistId, dispatch],
   )

   return (
      <div key={props.t.id} className={props.t.status === TaskStatuses.Completed ? 'is-done' : ''}>
         <Checkbox
            size={'small'}
            color={'primary'}
            onChange={onChangeHandler}
            checked={props.t.status === TaskStatuses.Completed}
         />
         <EditableSpan title={props.t.title} onChange={onChangeTitleHandler} />
         <IconButton size={'small'} onClick={onRemoveTaskHandler}>
            <Delete fontSize={'small'} />
         </IconButton>
      </div>
   )
})

export default Task
