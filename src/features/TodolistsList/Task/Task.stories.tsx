import React from 'react'
import Task from './Task'
import ProviderDecorator from '../../../stories/ProviderDecorator'
import { TaskPriority, TaskStatuses } from '../../../api/tasks-api'

export default {
   title: 'Task',
   component: Task,
   decorators: [ProviderDecorator],
}

export const TaskExample = () => {
   return (
      <>
         <Task
            key={1}
            t={{
               id: '1',
               title: 'CSS',
               status: TaskStatuses.Completed,
               description: '',
               addedDate: '',
               deadline: '',
               order: 0,
               startDate: '',
               priority: TaskPriority.Low,
               todoListId: 'todolistId1',
            }}
            todolistId={'todolistId1'}
         />
         <Task
            key={2}
            t={{
               id: '2',
               title: 'JS',
               status: TaskStatuses.New,
               description: '',
               addedDate: '',
               deadline: '',
               order: 0,
               startDate: '',
               priority: TaskPriority.Low,
               todoListId: 'todolistId1',
            }}
            todolistId={'todolistId2'}
         />
      </>
   )
}
