import { Dispatch } from 'redux'
import { authAPI, LoginParamsType } from '../../api/auth-API'
import { handleServerAppError, handleServerNetworkError } from '../../utils/handle-server-error'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { setStatusAC } from '../../app/app-reducer'

const authSlice = createSlice({
   name: 'auth',
   initialState: {
      isLoggedIn: false,
   },
   reducers: {
      setIsLoggedInAC(state, action: PayloadAction<{ value: boolean }>) {
         state.isLoggedIn = action.payload.value
      },
   },
})

export const authReducer = authSlice.reducer
export const { setIsLoggedInAC } = authSlice.actions

export const loginTC = (data: LoginParamsType) => async (dispatch: Dispatch) => {
   try {
      dispatch(setStatusAC({ status: 'loading' }))
      const response = await authAPI.login({ ...data })
      if (response.data.resultCode === 0) {
         dispatch(setIsLoggedInAC({ value: true }))
         dispatch(setStatusAC({ status: 'succeeded' }))
      } else {
         handleServerAppError(response.data, dispatch)
      }
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}

export const logOutTC = () => async (dispatch: Dispatch) => {
   try {
      dispatch(setStatusAC({ status: 'loading' }))
      const response = await authAPI.logOut()
      if (response.data.resultCode === 0) {
         dispatch(setIsLoggedInAC({ value: false }))
         dispatch(setStatusAC({ status: 'succeeded' }))
      } else {
         handleServerAppError(response.data, dispatch)
      }
   } catch (e) {
      handleServerNetworkError(e, dispatch)
   }
}
