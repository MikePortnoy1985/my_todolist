import { setErrorAC, setStatusAC } from '../app/app-reducer'
import { Dispatch } from 'redux'
import { ResponseType } from '../api/appTodolists-api'

export const handleServerAppError = <T>(data: ResponseType<T>, dispatch: Dispatch) => {
   if (data.messages.length) {
      dispatch(setErrorAC({ error: data.messages[0] }))
   } else {
      dispatch(setErrorAC({ error: 'Some error occurred' }))
   }
   dispatch(setStatusAC({ status: 'failed' }))
}

export const handleServerNetworkError = (error: { message: string }, dispatch: Dispatch) => {
   dispatch(setErrorAC(error.message ? { error: error.message } : { error: 'Some error occured' }))
   dispatch(setStatusAC({ status: 'failed' }))
}
