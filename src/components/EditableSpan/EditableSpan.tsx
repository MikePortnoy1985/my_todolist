import React, { ChangeEvent, useState } from 'react'
import { TextField } from '@material-ui/core'

type EditableSpanPropsType = {
   title: string
   onChange: (newValue: string) => void
}

export const EditableSpan = React.memo((props: EditableSpanPropsType) => {
   const [editMode, setEditMode] = useState(false)
   const [title, setTitle] = useState('')

   const activateEditMode = () => {
      setEditMode(true)
      setTitle(props.title)
   }
   const activateViewMode = () => {
      setEditMode(false)
      props.onChange(title)
   }
   const onChangeTitleHandler = (e: ChangeEvent<HTMLInputElement>) => {
      const title = e.currentTarget.value.trim()
      setTitle(title)
   }

   return editMode ? (
      <TextField
         variant={'outlined'}
         value={title.trim()}
         onBlur={activateViewMode}
         onChange={onChangeTitleHandler}
         autoFocus
         size={'small'}
      />
   ) : (
      <span onDoubleClick={activateEditMode}>{props.title}</span>
   )
})
