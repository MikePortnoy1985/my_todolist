import React, { useState } from 'react'
import { EditableSpan } from './EditableSpan'

export default {
   title: 'Editable Span',
   component: EditableSpan,
}

export const EditableSpanExample = () => {
   const [title, setTitle] = useState('React')

   return (
      <>
         <EditableSpan title={title} onChange={(e) => setTitle(e)} />
      </>
   )
}
