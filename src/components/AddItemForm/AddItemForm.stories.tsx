import React from 'react'
import { AddItemForm } from './AddItemForm'
import { action } from '@storybook/addon-actions'

export default {
   title: 'Add Item Form',
   component: AddItemForm,
}

export const AddItemFormExample = () => {
   return <AddItemForm addItem={action('New Todolist added')} />
}

export const AddItemFormDisabledExample = () => {
   return <AddItemForm disabled={true} addItem={action('New Todolist added')} />
}
