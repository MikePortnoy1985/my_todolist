import React, { ChangeEvent, KeyboardEvent, useState } from 'react'
import { IconButton, TextField } from '@material-ui/core'
import { PostAddSharp } from '@material-ui/icons'

type AddItemFormPropsType = {
   addItem: (title: string) => void
   disabled?: boolean
}

export const AddItemForm = React.memo(({ addItem, disabled = false }: AddItemFormPropsType) => {
   const [newTaskTittle, setNewTaskTitle] = useState('')
   const [error, setError] = useState<string | null>(null)

   const onNewTitleChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
      setNewTaskTitle(e.currentTarget.value)
   }

   const onKeyPressHandler = (e: KeyboardEvent<HTMLInputElement>) => {
      if (error !== null) {
         setError(null)
      }
      if (e.key === 'Enter') {
         addItem(newTaskTittle.trim())
         setNewTaskTitle('')
      }
   }

   const addTask = () => {
      if (newTaskTittle.trim() !== '') {
         addItem(newTaskTittle.trim())
         setNewTaskTitle('')
      } else {
         setError('Title is required')
      }
   }

   return (
      <div>
         <TextField
            disabled={disabled}
            variant={'outlined'}
            label={'Type value'}
            error={!!error}
            helperText={error}
            value={newTaskTittle}
            onChange={onNewTitleChangeHandler}
            onKeyPress={onKeyPressHandler}
         />
         <IconButton onClick={addTask} color='primary' size='small' disabled={disabled}>
            <PostAddSharp fontSize={'small'} />
         </IconButton>
      </div>
   )
})
