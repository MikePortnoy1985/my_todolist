import { appReducer, InitialStateType, setErrorAC, setStatusAC } from './app-reducer'

let startState: InitialStateType

beforeEach(() => {
   startState = {
      status: 'idle',
      error: null,
      isInitialized: false,
   }
})

test('correct error should be set', () => {
   const endState = appReducer(startState, setErrorAC({ error: 'some error' }))

   expect(endState.error).toBe('some error')
})

test('correct status should be set', () => {
   const endState = appReducer(startState, setStatusAC({ status: 'loading' }))

   expect(endState.status).toBe('loading')
})
