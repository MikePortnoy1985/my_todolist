import React from 'react'
import App from './App'
import ProviderDecorator from '../stories/ProviderDecorator'

export default {
   title: 'App',
   component: App,
   decorators: [ProviderDecorator],
}

export const AppBaseExample = (props: any) => {
   return <App />
}
