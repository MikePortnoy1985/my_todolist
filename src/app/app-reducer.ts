import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { authAPI } from '../api/auth-API'
import { setIsLoggedInAC } from '../features/Login/auth-reducer'
import { Dispatch } from 'redux'

export type RequestStatusType = 'idle' | 'loading' | 'succeeded' | 'failed'

export type InitialStateType = {
   status: RequestStatusType
   error: string | null
   isInitialized: boolean
}

const appSlice = createSlice({
   name: 'app',
   initialState: {
      status: 'idle',
      error: null,
      isInitialized: false,
   } as InitialStateType,
   reducers: {
      setStatusAC(state, action: PayloadAction<{ status: RequestStatusType }>) {
         state.status = action.payload.status
      },
      setErrorAC(state, action: PayloadAction<{ error: string | null }>) {
         state.error = action.payload.error
      },
      setInitializedAC(state, action: PayloadAction<{ isInitialized: boolean }>) {
         state.isInitialized = action.payload.isInitialized
      },
   },
})

export const appReducer = appSlice.reducer
export const { setStatusAC, setErrorAC, setInitializedAC } = appSlice.actions

export const initializeTC = () => async (dispatch: Dispatch) => {
   try {
      const response = await authAPI.me()
      if (response.data.resultCode === 0) {
         dispatch(setIsLoggedInAC({ value: true }))
      } else {
         dispatch(setIsLoggedInAC({ value: false }))
      }
   } catch (e) {
      dispatch(setIsLoggedInAC({ value: false }))
   } finally {
      dispatch(setInitializedAC({ isInitialized: true }))
   }
}
