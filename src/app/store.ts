import { combineReducers } from 'redux'
import { authReducer } from '../features/Login/auth-reducer'
import { tasksReducer } from '../features/TodolistsList/tasks-reducer'
import { todolistsReducer } from '../features/TodolistsList/todolists-reducer'
import { appReducer } from './app-reducer'
import { configureStore } from '@reduxjs/toolkit'

const rootReducer = combineReducers({
   tasks: tasksReducer,
   todolists: todolistsReducer,
   app: appReducer,
   authReducer: authReducer,
})

export const store = configureStore({
   reducer: rootReducer,
   middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
})

export type AppRootStateType = ReturnType<typeof rootReducer>
