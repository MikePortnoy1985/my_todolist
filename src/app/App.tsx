import React, { useCallback, useEffect } from 'react'
import './App.css'
import { AppBar, Button, IconButton, LinearProgress, Toolbar, Typography } from '@material-ui/core'
import { Menu } from '@material-ui/icons'
import TodolistsList from '../features/TodolistsList/CompTodolistsList'
import { ErrorSnackbar } from '../components/ErrorSnackBar/ErrorSnackBar'
import { useDispatch, useSelector } from 'react-redux'
import { AppRootStateType } from './store'
import { initializeTC, RequestStatusType } from './app-reducer'
import { Login } from '../features/Login/Login'
import { Route, Switch, Redirect, BrowserRouter } from 'react-router-dom'
import Container from '@material-ui/core/Container'
import CircularProgress from '@material-ui/core/CircularProgress'
import { logOutTC } from '../features/Login/auth-reducer'

function App() {
   const status = useSelector<AppRootStateType, RequestStatusType>((state) => state.app.status)
   const initialized = useSelector<AppRootStateType, boolean>((state) => state.app.isInitialized)
   const isLoggedIn = useSelector<AppRootStateType, boolean>((state) => state.authReducer.isLoggedIn)

   const dispatch = useDispatch()

   useEffect(() => {
      dispatch(initializeTC())
   }, [dispatch])

   const logoutHandler = useCallback(() => {
      dispatch(logOutTC())
   }, [dispatch])

   if (!initialized) {
      return <CircularProgress />
   }

   return (
      <div className='App'>
         <ErrorSnackbar />
         <AppBar position='static'>
            <Toolbar>
               <IconButton edge='start' color='inherit' aria-label='menu'>
                  <Menu />
               </IconButton>
               <Typography variant='h6'>Menu</Typography>
               {isLoggedIn && (
                  <Button color='inherit' onClick={logoutHandler}>
                     Log out
                  </Button>
               )}
            </Toolbar>
            {status === 'loading' && <LinearProgress />}
         </AppBar>
         <BrowserRouter>
            <Container fixed>
               <Switch>
                  <Route exact path={'/'} render={() => <TodolistsList />} />
                  <Route path={'/login'} render={() => <Login />} />
                  <Route exact path={'/404'} render={() => <h1>404: PAGE NOT FOUND</h1>} />
                  <Redirect from={'*'} to={'/404'} />
               </Switch>
            </Container>
         </BrowserRouter>
      </div>
   )
}

export default App
