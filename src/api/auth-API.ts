import axios from 'axios'
import { ResponseType } from './appTodolists-api'

export type AuthResponseType = {
   resultCode: number
   messages: string[]
   data: {
      userId: number
   }
}

export type LoginParamsType = {
   email: string
   password: string
   rememberMe: boolean
}

const instance = axios.create({
   baseURL: 'https://social-network.samuraijs.com/api/1.1',
   withCredentials: true,
   headers: {
      'API-KEY': '449db723-8aa5-493f-9ee5-86de5ac1dae8',
   },
})

export const authAPI = {
   login(data: LoginParamsType) {
      return instance.post<ResponseType<{ userId: number }>>(`/auth/login`, { ...data })
   },
   logOut() {
      return instance.delete<ResponseType<{ userId: number }>>(`/auth/login`)
   },
   me() {
      return instance.get<ResponseType<{ id: number; email: string; login: string }>>(`/auth/me`)
   },
}
