import axios from 'axios'

export type TodolistType = {
   id: string
   title: string
   addedDate: string
   order: number
}

export type ResponseType<D = {}> = {
   resultCode: number
   messages: Array<string>
   data: D
}

export const instance = axios.create({
   baseURL: 'https://social-network.samuraijs.com/api/1.1/todo-lists',
   withCredentials: true,
   headers: {
      'API-KEY': '449db723-8aa5-493f-9ee5-86de5ac1dae8',
   },
})

export const appTodolistsApi = {
   getTodolists() {
      return instance.get<Array<TodolistType>>('/')
   },
   createTodolist(title: string) {
      return instance.post<ResponseType<{ item: TodolistType }>>('/', { title })
   },
   deleteTodolist(todolistId: string) {
      return instance.delete<ResponseType>(`/${todolistId}`)
   },
   updateTodolistTitle(title: string, todolistId: string) {
      return instance.put<ResponseType>(`/${todolistId}`, { title })
   },
}
