import { ResponseType, instance } from './appTodolists-api'

export enum TaskStatuses {
   New = 0,
   InProgress = 1,
   Completed = 2,
   Draft = 3,
}

export enum TaskPriority {
   Low = 0,
   Middle = 1,
   High = 2,
   Urgently = 3,
   Later = 4,
}

export type TaskType = {
   description: string
   title: string
   status: TaskStatuses
   priority: TaskPriority
   startDate: string
   deadline: string
   id: string
   todoListId: string
   order: number
   addedDate: string
}

type GetTasksResponse = {
   error: string | null
   totalCount: number
   items: Array<TaskType>
}

export type UpdateTaskType = {
   title: string
   description: string
   status: TaskStatuses
   priority: TaskPriority
   startDate: string
   deadline: string
}

export const tasksAPI = {
   getTasks(todolistId: string) {
      return instance.get<GetTasksResponse>(`/${todolistId}/tasks`)
   },
   createTask(todolistId: string, title: string) {
      return instance.post<ResponseType<{ item: TaskType }>>(`/${todolistId}/tasks`, { title })
   },
   deleteTask(todolistId: string, taskId: string) {
      return instance.delete<ResponseType>(`/${todolistId}/tasks/${taskId}`)
   },
   updateTask(todolistId: string, taskId: string, model: UpdateTaskType) {
      return instance.put(`/${todolistId}/tasks/${taskId}`, model)
   },
}
